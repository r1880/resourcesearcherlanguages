package es.resource.searcher.route;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import es.resource.searcher.constants.LanguageConstants;
import es.resource.searcher.entities.Language;
import es.resource.searcher.service.LanguageService;
import es.valhalla.business.component.AbstractBaseHttpHandler;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.exception.ValhallaBusinessExceptionEnum;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class LanguageEndpoint extends AbstractBaseHttpHandler {

	@Inject
	LanguageService languageService;

	private Map<String, List<ValhallaServiceRegistry>> serviceRegistry;

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(Language.class);

	private static enum ENDPOINTS {
		INSERT, ALL, DELETE
	};

	public void getAll(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.GET.name(),
				LanguageConstants.SERVICE_PORT, LanguageConstants.SERVICE_PATH, getSelfAddress());
		serviceRegistry.get(LanguageConstants.APP_NAME).add(self);
		buildRouteEmpty(router, LanguageConstants.SERVICE_PATH, HttpMethod.GET, MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.ALL)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	}

	public void insertTechnology(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(),
				LanguageConstants.SERVICE_PORT, LanguageConstants.SERVICE_PATH, getSelfAddress());
		serviceRegistry.get(LanguageConstants.APP_NAME).add(self);
		buildRoute(router, LanguageConstants.SERVICE_PATH, HttpMethod.POST,MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.INSERT)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	public void deleteTechnology(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.DELETE.name(),
				LanguageConstants.SERVICE_PORT, LanguageConstants.SERVICE_PATH, getSelfAddress());
		serviceRegistry.get(LanguageConstants.APP_NAME).add(self);
		buildRoute(router, LanguageConstants.SERVICE_PATH, HttpMethod.DELETE,MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.DELETE)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	private void proccess(final RoutingContext ctx, final ENDPOINTS endpoint) {
		final long init = System.currentTimeMillis();
		final String token = getTokenFromHeader(ctx.request());
		if (token != null) {
			final ValhallaServiceRegistry idpService = serviceRegistry.get(ValahallaConstants.AUTH_SERVICE).stream()
					.filter(s -> s.getServicePath().contains("validate")).findFirst().orElse(null);
			if (idpService == null) {
				LOGGER.error("IDP not registered");
				throw ValhallaBusinessExceptionEnum.IDP_NOT_REGISTERED.getException();
			}
			idpValidation(idpService.getServiceAddress(), idpService.getServicePath(), idpService.getPort(), token)
					.onSuccess(handler -> {
						if (handler.statusCode() == HttpResponseStatus.OK.code()) {

							final String serialized = executedOption(ctx, endpoint);
							processSuccessHttpResponse(ctx, serialized);
						} else {
							processUnauthorizedResponse(ctx);
						}

					}).onFailure(handler -> {
						processUnauthorizedResponse(ctx);
					}).onComplete(
							finished -> LOGGER.info("Request processed in {} ms", System.currentTimeMillis() - init));
		} else {
			processUnauthorizedResponse(ctx);
		}
	}

	private String executedOption(final RoutingContext ctx, final ENDPOINTS endpoint) {
		String serialized = null;
		switch (endpoint) {

		case ALL:
			serialized = serialize(languageService.getLanguages());
			break;
		case INSERT:
			languageService.insert(deserialize(ctx.getBodyAsString(), Language.class));
			break;

		case DELETE:
			languageService.delete(deserialize(ctx.getBodyAsString(), Language.class));

			break;

		}
		return serialized;
	}

	public void enableEndpoints(final Router router, final Map<String, List<ValhallaServiceRegistry>> serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
		serviceRegistry.put(LanguageConstants.APP_NAME, new ArrayList<>());
		getAll(router);
		insertTechnology(router);
		deleteTechnology(router);
	}

}
