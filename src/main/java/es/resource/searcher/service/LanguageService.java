package es.resource.searcher.service;

import java.util.List;

import es.resource.searcher.entities.Language;


public interface LanguageService {

	
	 void insert(Language language);

	 void delete(Language language );
	
	 List<Language> getLanguages();
	
	 
	 
	
}
