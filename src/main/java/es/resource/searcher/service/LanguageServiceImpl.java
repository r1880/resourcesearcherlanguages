package es.resource.searcher.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.resource.searcher.constants.LanguageConstants;
import es.resource.searcher.entities.Language;
import es.resource.searcher.entities.Resource;
import es.valhalla.business.component.AbstractBusinessComponent;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.data.access.mongodb.entities.BaseEntity;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import jakarta.annotation.PostConstruct;

public class LanguageServiceImpl extends AbstractBusinessComponent implements LanguageService {

	private String url;
	private String databaseName;
	private String user;
	private String secret;
	private String collection;
	
	final ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(LanguageServiceImpl.class);


	@PostConstruct
	private void init() {
		this.url = getStringProperty(LanguageConstants.APP_NAME, ValahallaConstants.MONGO_URL);
		this.databaseName = getStringProperty(LanguageConstants.APP_NAME, ValahallaConstants.MONGO_DBNAME);
		this.user = getStringProperty(LanguageConstants.APP_NAME, ValahallaConstants.MONGO_USER);
		this.secret = getStringProperty(LanguageConstants.APP_NAME, ValahallaConstants.MONGO_SECRET);
		this.collection = getStringProperty(LanguageConstants.APP_NAME, LanguageConstants.COLLECTION_NAME);
	}

	@Override
	public void insert(Language language) {
		language.setId(language.getName());
		insertMongo(url, user, secret, databaseName, collection, language, Language.class);
	}

	@Override
	public void delete(Language language) {
		deleteMongo(url, user, secret, databaseName, collection, language.getId(), Language.class);
		List<Resource> resourcesToUpdate = findResources(resourceForQuery(language));
		resourcesToUpdate.forEach(r -> {
			LOGGER.info("updating resource {} {} removing language {}", r.getId(), r.getName(), language.getName());
			r.setLanguages(r.getLanguages().stream().filter( l-> !l.getId().equals(language.getId()))
					.collect(Collectors.toList()));
			updateMongo(url, user, secret, databaseName, LanguageConstants.MONGO_RESOURCES, r, Resource.class);
		});
	}

	@Override
	public List<Language> getLanguages() {
		return getAllMongo(url, user, secret, databaseName, collection, Language.class);
	}

	
	private Resource resourceForQuery(final Language langId) {
		final Resource resource = new Resource();
		resource.setLanguages(new ArrayList<>() {
			{
				add(langId);
			}
		});
		return resource;
	}

	private List<Resource> findResources(final Resource resource) {

		final Map<String, Object> mapForMongo = new HashMap<>();

		Map<String, Object> langsForFetch = fetchFromArray(resource.getLanguages());
		if (langsForFetch != null)
			mapForMongo.put("languages._id", langsForFetch);

		return find(url, user, secret, databaseName, LanguageConstants.MONGO_RESOURCES, Resource.class, mapForMongo);

	}

	private <T extends BaseEntity> Map<String, Object> fetchFromArray(List<T> items) {
		Map<String, Object> contains = null;

		if (items != null && !items.isEmpty()) {
			contains = new HashMap<>();
			List<String> fields = new ArrayList<>();
			for (T item : items) {
				fields.add(item.getId());
			}
			contains.put(LanguageConstants.MONGO_ALL, fields);
		}
		return contains;

	}
}


