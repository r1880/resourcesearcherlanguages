package es.resource.searcher.launcher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import es.resource.searcher.constants.LanguageConstants;
import es.resource.searcher.route.LanguageEndpoint;
import es.valhalla.business.component.ValhallaServiceRegistryService;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.data.access.mongodb.service.MongoService;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.utils.configuration.application.service.ConfigurationApplicationService;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import jakarta.enterprise.inject.spi.CDI;

@ApplicationScoped
public class VertxIntializer {

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(VertxIntializer.class);
	private Vertx vertx;
	private ConfigurationApplicationService configurationApplicationService;
	private String serviceRegistryPath;
	private Map<String, List<ValhallaServiceRegistry>> serviceRegistry = new HashMap<>();


	public void itnitalizeVertx() {
		vertx = Vertx.vertx();
		serviceRegistryPath = System.getProperty(LanguageConstants.SERVICE_REGISTRY_PROP);

		serviceRegistryPath = serviceRegistryPath != null ? serviceRegistryPath : LanguageConstants.DEFAULT_REGISTRY_PATH;
		readProperties();
		openMongoConnection();
		intializeHttpServer();
		ValhallaServiceRegistryService.selfRegister(vertx, serviceRegistry, serviceRegistryPath,
				LanguageConstants.APP_NAME);
	}

	private void intializeHttpServer() {

		LOGGER.info("Deploying http server");
		HttpServer server = vertx.createHttpServer();
		Router router = Router.router(vertx);
		CDI.current().select(LanguageEndpoint.class).get().enableEndpoints(router,serviceRegistry);
		server.requestHandler(router).listen(LanguageConstants.SERVICE_PORT);
		LOGGER.info("Listening on: {}", LanguageConstants.SERVICE_PORT);

	}

	private void readProperties() {
		LOGGER.info("Initializing properties");
		configurationApplicationService = CDI.current().select(ConfigurationApplicationService.class).get();
		configurationApplicationService.readProperties(LanguageConstants.APP_NAME);
	}

	private void openMongoConnection() {
		LOGGER.info("Initializing mongo connection");
		final MongoService mongoService = CDI.current().select(MongoService.class).get();
		mongoService.intializeConnection(
				configurationApplicationService.getStringProperty(LanguageConstants.APP_NAME,
						ValahallaConstants.MONGO_URL),
				configurationApplicationService.getStringProperty(LanguageConstants.APP_NAME,
						ValahallaConstants.MONGO_USER),
				configurationApplicationService.getStringProperty(LanguageConstants.APP_NAME,
						ValahallaConstants.MONGO_SECRET),
				configurationApplicationService.getStringProperty(LanguageConstants.APP_NAME,
						ValahallaConstants.MONGO_DBNAME));
	}

}
