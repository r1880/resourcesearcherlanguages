package es.resource.searcher.launcher;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import es.valhalla.constants.ValahallaConstants;

public class Main {
	


	public static void main(String[] args) {
		

		final String logPath= System.getProperty(ValahallaConstants.VALHALLA_LOG_PATH);
		final String logName= System.getProperty(ValahallaConstants.VALHALLA_LOG_NAME);
		
		if(logPath==null) {
			System.setProperty(ValahallaConstants.VALHALLA_LOG_PATH, "");
		}
		if(logName==null) {
			System.setProperty(ValahallaConstants.VALHALLA_LOG_NAME, "languages");
		}	
		final Weld weld = new Weld();
		final WeldContainer container = weld.initialize();
		container.select(VertxIntializer.class).get().itnitalizeVertx();
		
	}

}
