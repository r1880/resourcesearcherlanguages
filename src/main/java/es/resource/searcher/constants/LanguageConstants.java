package es.resource.searcher.constants;

public class LanguageConstants {

	public static final String APP_NAME = "Languages";
	public static final String SERVICE_PATH = "/resourcesearcher/rest/service/languages";
	public static final int SERVICE_PORT = 8083;
	public static final String COLLECTION_NAME = "mongo.collection";
	public static final String ID = "id";
	public static final String SERVICE_REGISTRY_PROP = "service.registry.path";
	public static final String MONGO_ALL = "$all";
	public static final String MONGO_RESOURCES = "resources";
	/** LOCAL PROPERTY **/
	public static final String DEFAULT_REGISTRY_PATH = "C:\\ResoureSearcher\\resource_searcher.json";
}
