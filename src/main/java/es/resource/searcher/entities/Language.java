package es.resource.searcher.entities;

import es.valhalla.data.access.mongodb.entities.BaseEntity;

public class Language extends BaseEntity {

	private String name;

	public Language() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
